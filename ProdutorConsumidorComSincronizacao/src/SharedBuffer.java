import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SharedBuffer implements Buffer {
    public final String NOWORD = ":NO_WORD:";
    // permite que apenas uma thread acesse os recursos
    private Lock lock = new ReentrantLock();
    // utilizado, nesta aplicação, para colocar a thread do produtor em estado de espera e retirala desse mesmo estado
    private Condition conditionToWrite = lock.newCondition();
    // utilizado, nesta aplicação, para colocar a thread do consumidor em estado de espera e retirala desse mesmo estado
    private Condition conditionToRead = lock.newCondition();
    // variavel que informará se o buffer está cheio ou não
    private boolean fullBuffer = false;

    // Caso não exista palavra atribuida pelo produtor, o consumidor vai recuperar o valor default: "::::::no word:::::"
    public String word = NOWORD;


    // Implementação do método get da interface Buffer. Será utilizado pelo consumidor para recuperar as palavras
    @Override
    public String get() {
        String value = NOWORD;
        // quando a thread do consumidor chegar nesse ponto, a thread do produtor não poderar ter acesso aos recursos até que o lock seja liberado
        lock.lock();
        try {
            // caso a thread do consumidor obtenha o lock enquanto não existe nenhum valor no buffer ele entrará na condicional
            while(!fullBuffer){
                System.out.println("Empty buffer. Consumer await.");
                // a thread do consumidor será posta em estado de espera até que a condição seja mudada pela thread do producer
                conditionToRead.await();
            }
            value = word;
            // a variável que indica o buffer cheio foi atribuido o valor false pois o valor foi lido pela thread do consumer
            fullBuffer = false;
            System.out.println("                    Consumer: "+value);
            // a thread do producer e removida do estado de espera
            conditionToWrite.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }
        return value;
    }

    // Implementação do método ser da interface Buffer. Será utilizado pelo produtor para atribuir uma palavra
    @Override
    public void set(String value) {
        // quando a thread do produtor chegar nesse ponto, a thread do consumidor não poderar ter acesso aos recursos até que o lock seja liberado
        lock.lock();
        try {
            // caso a thread do produtor obtenha o lock enquanto existe um valor no buffer ele entrará na condicional
            while(fullBuffer){
                System.out.println("Full buffer. Producer await.");
                // a thread do produtor será posta em estado de espera até que a condição seja mudada pela thread do consumer
                conditionToWrite.await();
            }
            this.word = value;
            // a variável que indica o buffer cheio foi dado o valor true pois o buffer possui um novo valor
            fullBuffer = true;
            System.out.println("Producer:"+value);
            // a thread do consumer foi tirada do modo de espera
            conditionToRead.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            // para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }
    }
}
