
### Para executar a aplicação, no terminal, dentro da pasta da aplicação, execute os seguintes comandos: 

1. `cd src`
2. `javac Main.java`
3. `java Main "testando" "a" "ordem" "das" "palavras" "na" "frase" ":)"`
    - A aplicação consiste em executar um produtor de palavras bem como um consumidor em duas threads.
    - Os parametros passados são palavras que serão utilizadas para formar uma frase na ordem(menos para o primeiro exemplo) em que foram informadas.
